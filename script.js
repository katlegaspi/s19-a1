// cube
const firstNum = Math.pow(2, 3);
const getCube = `The cube of 2 is ${firstNum}.`
console.log(getCube)

// address
const fullAddress = ["258 Washington Ave", "NW", "California", "90011"]
const [street, direction, state, zipCode] = fullAddress
console.log(`I live at ${street} ${direction}, ${state}, ${zipCode}.`)

// croc
const animal = {
	name: "Lolong",
	species: "saltwater crocodile",
	weight: "1075 kgs",
	length: "20 ft 3 in"
}
const {name, species, weight, length} = animal;
console.log(`${name} was a ${species}. He weighed at ${weight} with a measurement of ${length}.`);

// doggo
class Dog{
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}
const dogA = new Dog("Frankie", "5", "Miniature Dachshund");
console.log(dogA);